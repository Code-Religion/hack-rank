# Q.7 Given an integer,n , and n space-separated integers as input, create a tuple ,t , of those n  integers. Then compute and print the result of hash(t).

from builtins import hash
list1=[]
if __name__ == '__main__':
   n = int(input())
   integer_list = map(int, input().split(" "))
for i in integer_list:
   list1.append(i)

print(hash(tuple(list1)))
