
def area(x1, y1, x2, y2, x3, y3):
    return abs((x1 * (y2 - y3) + x2 * (y3 - y1)
                + x3 * (y1 - y2)) / 2.0)

def containsOrigin(x1, y1, x2, y2, x3, y3):

    x = 0
    y = 0

    # Calculate area of triangle ABC
    A = area(x1, y1, x2, y2, x3, y3)

    # Calculate area of triangle PBC
    A1 = area(x, y, x2, y2, x3, y3)

    # Calculate area of triangle PAC
    A2 = area(x1, y1, x, y, x3, y3)

    # Calculate area of triangle PAB
    A3 = area(x1, y1, x2, y2, x, y)

    # Check if sum of A1, A2 and A3
    # is same as A
    if (A == A1 + A2 + A3):
        return True
    else:
        return False

f = open("triangles.txt", "r")
triangles = f.readlines()

containsOriginCount = 0
for triangle in triangles:
    t = eval(triangle.strip("\n"))

    if (containsOrigin(*t)):
        containsOriginCount = containsOriginCount + 1
        print('Origin is Inside the triangle ', t)
    else:
        print('Origin is not Inside the triangle ',t)


print("\n\n--------------------------------------------------------------------------------------------- ")

print("\nThe number of triangles : ",len(triangles) )
print("\nThe number of triangles for which the interior does not contains the origin : ", len(triangles) - containsOriginCount)
print("\nThe number of triangles for which the interior contains the origin : ",containsOriginCount,"\n\n" )