# Q
# .5
# Given
# the
# participants
# ' score sheet for your University Sports Day, you are required to find the runner-up score. You are given scores. Store them in a list and find the score of the runner-up.

if __name__ == '__main__':
    n = int(input())
    arr = map(int, input().split())

l = set()
# for i in arr:
#     if i not in l:
#         l.append(i)
l.update((i for i in arr))
l = sorted(l)
l.remove(max(l))
print(max(l))
