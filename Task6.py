# Q.6 Given the names and grades for each student in a Physics class of  N students, store them in a nested list and print the name(s) of any student(s) having the second lowest grade.
# Note: If there are multiple students with the same grade, order their names alphabetically and print each name on a new line.
# Input Format:
# The first line contains an integer, N,the number of students.
# The 2N  subsequent lines describe each student over 2 lines; the first line contains a student's name, and the second line contains their grade.

dict1={}
list2=set()
list3=[]
if __name__ == '__main__':
   for _ in range(int(input())):
       name = input()
       score = float(input())
       dict1[name]=score
       #list1.add("name":score)
       list2.add(score)
# list1.sort()
list2.remove(min(list2))
a=min(list2)
for k,v in dict1.items():
   if v == a:
       list3.append(k)
list3.sort()
for i in list3:
   print("".join(map(str,(i))))
